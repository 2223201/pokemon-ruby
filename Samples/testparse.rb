# frozen_string_literal: true

require 'nokogiri'
require 'nokogiri-pretty'

# cleaner version of xml_parser.rb (probably)
class TestParse
  def initialize
    @student_doc = Nokogiri::XML(File.open('resources/students.xml'))
    @prof_doc = Nokogiri::XML(File.open('resources/professors.xml'))
  end

  # used to get student details
  def get_student(student_id)
    # @course_to_edit = @student_doc.at("//Student[@id=\"#{student_id}\"]/courses/course[@id=\"#{cours_id}\"]")
    @student_to_edit = 0
    @student_to_edit = @student_doc.at("//Student[@id=\"#{student_id}\"]")
    # puts @student_to_edit

    # check if student exists
    return if @student_to_edit

    puts "Student doesn't exist"
  end

  # used to get prof details
  def get_prof(prof_id)
    @prof_to_edit = @prof_doc.at("//prof[@id=\"#{prof_id}\"]")
    # puts @prof_to_edit
    # check if prof exists
    return if @prof_to_edit

    puts "Professor doesn't exist"
  end

  # feels like may more efficient way para gawin to
  def get_student_course(course_id)
    @course_to_edit = @student_to_edit.at("courses/course[@id=\"#{course_id}\"]")
    puts @course_to_edit

    # check if course exists
    return if @course_to_edit

    puts "Course doesn't exist"
  end

  def edit_student_course_name(new_course_name)
    @course_to_edit.at('courseName').content = new_course_name
    # puts course_to_edit
    File.open('resources/students.xml', 'w') { |file| file.write(@student_doc) }
  end

  def edit_grade_prelims(new_grade)
    @course_to_edit.at('grades/pgrade').content = new_grade
    # puts course_to_edit
    File.open('resources/students.xml', 'w') { |file| file.write(@student_doc) }
  end

  def edit_grade_midterms(new_grade)
    @course_to_edit.at('grades/mgrade').content = new_grade
    # puts course_to_edit
    File.open('resources/students.xml', 'w') { |file| file.write(@student_doc) }
  end

  def edit_grade_finals(new_grade)
    @course_to_edit.at('grades/fgrade').content = new_grade
    # puts course_to_edit
    File.open('resources/students.xml', 'w') { |file| file.write(@student_doc) }
  end



  # def add_new_course_student(course_id, units, course_name)
  #   course_node = create_new_course(course_id, units, course_name)
  #   course_list = @student_to_edit.at('courses')
  #   course_list.add_child(course_node)
  # end

  # private

  def create_new_course_student(course_id, units, course_name)
    # Turn the xml into literal text
    new_course_xml = Nokogiri::XML::Builder.new do |xml|
      xml.course('id' => course_id, 'units' => units) do
        xml.courseName course_name
        xml.grades do
          xml.pgrade 0
          xml.mgrade 0
          xml.fgrade 0
        end
      end
    end.to_xml

    puts "Student id is: #{@student_to_edit}"
    # Manually load the course so we know where we are suppose to slap the new text
    courses_element = @student_to_edit.at('courses')
    new_course_xml = add_indentation_to_string(new_course_xml,6)
    #  We remove the first line because the the first line is literally <?xml version="1.0"?>
    courses_element << ("\n" + new_course_xml.split("\n")[1..-1].join("\n") + "\n")

    # save to xml
    File.open('resources/students.xml', 'w') do |file|
      file.write(@student_doc.to_xml(indent: 2))
    end
  end

  def add_new_course_prof(course_id)
    new_course_xml = Nokogiri::XML::Builder.new do |xml|
      xml.course course_id
    end.to_xml

    puts "Prof ID is: #{@prof_to_edit}"

    courses_element = @prof_to_edit.at('units')
    new_course_xml = add_indentation_to_string(new_course_xml, 2)
    courses_element << ("\n" + new_course_xml.split("\n")[1..-1].join("\n") + "\n")

    File.open('resources/students.xml', 'w') do |file|
      file.write(@prof_doc.to_xml(indent: 2))
    end
  end
end

def add_indentation_to_string(input_string, num_spaces)
  indent = ' ' * num_spaces
  indented_string = input_string.gsub(/^/, indent)
end

rey = TestParse.new
# rey.get_student(2220973)
# rey.get_student_course(9405)
# rey.edit_student_course_name('sigepards')

# dito problema boss tingen here
rey.get_student(2227854)
# rey.get_student_course(9405)
# rey.edit_student_course_name('haha')
# rey.get_prof(999123)
rey.create_new_course_student(8989, 2, 'sigepare')
rey.get_student_course(8989)

rey.get_prof(999789)
rey.add_new_course_prof(9982)
# rey.add_new_course_student(9999, 'realcamzahours')
# rey.get_student_course(9999)
# rey.edit_student_course_name('realcamzahours')

