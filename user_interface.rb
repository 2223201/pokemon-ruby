require_relative 'Objects/xml_parser.rb'

class UserInterface

  def initialize
    @xml_parser = XmlParser.new
    @user
  end
  def start
    username1 = 1
    loop do
      print 'Username: '
      username2 = gets.chomp.to_i
      print 'Password: '
      password = gets.chomp.to_s
      username1 = username2
      @user = account_verification(username2, password)
        break if @user
        puts "Invalid credentials. Please try again."
      end
    if username1.to_s.start_with?("222")
      student_home
    elsif username1.to_s.start_with?("999")
      faculty_home
    end
  end

  # Checks if the account exists in the database.
  # If the account exists it calls the check_account_status method
  # to check whether the account is from a student or a faculty member
  # @param username, password, status [String, String, String]
  def account_verification(username, password)
    @xml_parser.login(username, password)
  end

  def logout
    @user = nil
    self.start
  end

  # Displays the home interface for the student
  def student_home
    while true
      student_main_menu
      print "Choice: "
      choice = gets.chomp.to_i
      case choice
      when 1
        student_check_grades
      when 2
        puts student_enrolled_courses
      when 3
        puts "LOGGED OUT!"
        logout
      else
        puts 'Invalid choice.'
      end
    end
  end

  # Displays the main menu choices for the student
  def student_main_menu
    puts '1. Check grades'
    puts '2. Enrolled Courses'
    puts '3. Logout'
  end

  # Displays the grades of the student
  def student_check_grades
    puts @xml_parser.student_info(@user.to_s)
  end

  # Displays the enrolled courses of the student
  def student_enrolled_courses
    puts @xml_parser.get_enrolled_courses(@user.to_s)
  end

  # Displays the home interface for the faculty


  def check_grade_menu
    class_codes = @xml_parser.get_class_codes(@user)
    if class_codes.empty?
      puts "No class codes available."
    else
      puts "Select a class code:"
      class_codes.each_with_index do |class_code, index|
        puts "#{index + 1}. #{class_code}"
      end

      print "Choice: "
      class_choice = gets.chomp.to_i

      if class_choice >= 1 && class_choice <= class_codes.length
        selected_class_code = class_codes[class_choice - 1]
        puts
        header = sprintf(
          "%-10s%-70s%-6s%-13s%-13s%-13s%-10s\n",
          "CODE", "COURSE", "UNITS", "PRELIM", "MIDTERM", "FINALS", "FINAL GRADE#{@finals_grade}"
        )
        puts header
        puts @xml_parser.get_all_grades_class(selected_class_code)
        puts

      else
        puts "Invalid choice."
      end
    end
  end

  def encode_grades
    class_codes = @xml_parser.get_class_codes(@user)
    if class_codes.empty?
      puts "No class codes available."
    else
      puts "Select a class code:"
      class_codes.each_with_index do |class_code, index|
        puts "#{index + 1}. #{class_code}"
      end

      print "Choice: "
      class_choice = gets.chomp.to_i

      if class_choice >= 1 && class_choice <= class_codes.length
        selected_class_code = class_codes[class_choice - 1]
        encode_grades_for_class(selected_class_code)
      else
        puts "Invalid choice."
      end
    end
  end
  def encode_grades_for_class(class_code)
    puts "ENCODING GRADES FOR CLASS: #{class_code}"

    students_ids = @xml_parser.get_students_of_class(class_code)

    if students_ids.empty?
      puts "No students found for class code #{class_code}."
      return
    end

    puts "Select the type of grade to encode:"
    puts "1. Prelim"
    puts "2. Midterm"
    puts "3. Finals"
    print "Choice: "
    grade_type_choice = gets.chomp.to_i

    unless [1, 2, 3].include?(grade_type_choice)
      puts "Invalid choice."
      return
    end

    grade_type = case grade_type_choice
                 when 1 then "Prelim"
                 when 2 then "Midterm"
                 when 3 then "Finals"
                 else
                   return
                 end

    students_ids.each do |student_id|
      loop do
        print "Enter #{grade_type} grade for Student ID #{student_id}: "
        grade = gets.chomp.to_i

        if (65..99).include?(grade)
          # Here, you need to update the grade in your XML data using your XMLParser.
          # You can use the `update_grades` method we discussed earlier.
          @xml_parser.edit_grade(student_id, class_code, grade_type, grade)
          puts "Grade saved."
          break
        else
          puts "Invalid grade. Grades must be between 65 and 99."
        end
      end
    end

    puts "Grade encoding complete for Class Code #{class_code}."
  end


  def edit_grades
    class_codes = @xml_parser.get_class_codes(@user)
    if class_codes.empty?
      puts "No class codes available."
      return
    end

    puts "Select a class code:"
    class_codes.each_with_index do |class_code, index|
      puts "#{index + 1}. #{class_code}"
    end

    print "Choice: "
    class_choice = gets.chomp.to_i

    if class_choice >= 1 && class_choice <= class_codes.length
      selected_class_code = class_codes[class_choice - 1]

      puts "Select the type of grade to edit:"
      puts "1. Prelim"
      puts "2. Midterm"
      puts "3. Finals"
      print "Choice: "
      grade_type_choice = gets.chomp.to_i

      unless [1, 2, 3].include?(grade_type_choice)
        puts "Invalid choice."
        return
      end

      grade_type = case grade_type_choice
                   when 1 then "Prelim"
                   when 2 then "Midterm"
                   when 3 then "Finals"
                   else
                     return
                   end

      student_ids = @xml_parser.get_students_of_class(selected_class_code)

      if student_ids.empty?
        puts "No students found for class code #{selected_class_code}."
        return
      end

      puts "Select a student ID:"
      student_ids.each_with_index do |student_id, index|
        puts "#{index + 1}. #{student_id}"
      end

      print "Choice: "
      student_choice = gets.chomp.to_i

      if student_choice >= 1 && student_choice <= student_ids.length
        student_id = student_ids[student_choice - 1]

        loop do
          print "Enter new #{grade_type} grade for Student ID #{student_id}: "
          new_grade = gets.chomp.to_i

          if (65..99).include?(new_grade)
            @xml_parser.edit_grade(student_id, selected_class_code, grade_type, new_grade)
            puts "Grade updated."
            break
          else
            puts "Invalid grade. Grades must be between 65 and 99."
          end
        end
      else
        puts "Invalid choice."
      end
    else
      puts "Invalid choice."
    end
  end


  # Displays the main menu choices of the faculty member
  def faculty_main_menu
    puts "1. Check grades in class"
    puts "2. Encode grades"
    puts "3. Edit grades for a student"
  end

  def faculty_home
    while true
      faculty_main_menu
      print "Choice: "
      choice = gets.chomp.to_i
      case choice
      when 1
        check_grade_menu
      when 2
        encode_grades
      when 3
        edit_grades
      when 4
        logout
      else
        puts 'Invalid choice.'
      end
    end
  end
end