class Professor
  attr_accessor :id, :name, :units_array
  attr_reader :units_array

  def initialize(id, name)
    @id = id
    @name = name
    @units_array = []
  end

  def add_units(units_arr)
    @units_array << units_arr
  end
  def to_s
    faculty_info = <<~HEREDOC
      FACULTY ID: #{@id}
      NAME: #{@name}
    HEREDOC

    units_info = @units_array.map do |units|
      "UNITS: #{units}"
    end.join("\n")

    "#{faculty_info}#{units_info}\n\n"
  end
end