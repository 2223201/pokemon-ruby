# frozen_string_literal: true

#assumption ->
class Grades
  attr_accessor :classroom_id, :course_name, :units, :prelim_grade, :midterm_grade, :tentative_fgrade, :finals_grade

  def initialize(xml_parser, classroom_id, course_name, units, prelim_grade = nil, midterm_grade = nil, tentative_fgrade = nil)
    @xml_parser = xml_parser
    @classroom_id = classroom_id
    @course_name = course_name
    @units = units
    @prelim_grade = prelim_grade
    @midterm_grade = midterm_grade
    @tentative_fgrade = tentative_fgrade

    if @prelim_grade.nil? || @midterm_grade.nil? || @tentative_fgrade.nil?
      @finals_grade = "N/A"
    else
      @finals_grade = compute_final_grade
    end

  end

  def compute_final_grade
      weights = @xml_parser.get_course_weights(classroom_id)

      prelim_weight = weights[:prelim_weight]
      midterm_weight = weights[:midterm_weight]
      finals_weight = weights[:finals_weight]

      final_grade = (prelim_grade * prelim_weight + midterm_grade * midterm_weight + tentative_fgrade * finals_weight) / 100.0
      final_grade
  end

  # might b useful later:)
  # def to_s
  #
  #   "Class Code: #{classroom_id.to_s.ljust(9)}" \
  #     "Course: #{course_name.ljust(70)}" \
  #     "Units: #{units.to_s.ljust(6)}" \
  #     "Prelim Grade: #{prelim_grade.to_s.ljust(14)}" \
  #     "Midterm Grade: #{midterm_grade.to_s.ljust(15)}" \
  #     "Tentative Final Grade: #{tentative_fgrade.to_s.ljust(22)}" \
  #     "Final Grade: #{@finals_grade}"
  #
  # end

  def to_s

    "#{classroom_id.to_s.ljust(10)}" \
      "#{course_name.ljust(70)}" \
      "#{units.to_s.ljust(6)}" \
      "#{prelim_grade.to_s.ljust(13)}" \
      "#{midterm_grade.to_s.ljust(13)}" \
      "#{tentative_fgrade.to_s.ljust(13)}" \
      "#{@finals_grade}"

  end
end

