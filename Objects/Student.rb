class Student
  attr_accessor :id, :name, :grades_arr, :pass

  def initialize(id, name)
    @id = id
    @name = name
    @grades_arr = []
  end

  def add_grade(grades)
    @grades_arr << grades
  end

  def to_s
    student_info = sprintf("Student ID: %s\nName: %s\n", id, name)

    header = sprintf(
      "%-10s%-70s%-6s%-13s%-13s%-13s%-10s\n",
      "CODE", "COURSE", "UNITS", "PRELIM", "MIDTERM", "FINALS", "FINAL GRADE#{@finals_grade}"
    )

    grades_info = ""
    grades_arr.each do |grades|
      grades_info += "#{grades.to_s}\n"
    end

    student_info + header + grades_info +"\n\n"
  end
end
